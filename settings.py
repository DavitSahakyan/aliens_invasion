class Settings():
    def __init__(self):
        self.screen_width = 1000
        self.screen_heigth = 600
        self.bg_color = (0, 0, 255)

        self.ship_speed = 2
        self.ship_limit = 3

        self.bullet_width = 5
        self.bullet_height = 25
        self.bullet_color = (60, 60, 60)
        self.bullet_allowed = 3

        self.fleet_drop_speed = 10
        self.fleet_direction = 1

        self.speedup_scale = 1.5
        self.score_scale = 1.5

        self.initialize_dynamic_settings()

    def initialize_dynamic_settings(self):
        self.bullet_speed = 0.5
        self.alien_speed = 0.5
        self.alien_point = 50

    def increase_speed(self):
        self.bullet_speed *= self.speedup_scale
        self.alien_speed *= self.speedup_scale
        self.alien_point *= 2 
